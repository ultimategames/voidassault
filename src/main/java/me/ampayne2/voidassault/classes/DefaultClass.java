/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.classes;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.ArenaStatus;
import me.ampayne2.ultimategames.api.games.Game;
import me.ampayne2.ultimategames.api.games.items.GameItem;
import me.ampayne2.ultimategames.api.players.classes.ClassSelector;
import me.ampayne2.ultimategames.api.players.teams.TeamSelector;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class DefaultClass extends VoidClass {
    private UltimateGames ultimateGames;
    private Game game;
    private GameItem teamSelector;
    private GameItem classSelector;

    public DefaultClass(UltimateGames ultimateGames, Game game) {
        super(ultimateGames, game, "Default", Resistance.MEDIUM, WalkSpeed.MEDIUM, 0, false);
        this.ultimateGames = ultimateGames;
        this.game = game;
        teamSelector = new TeamSelector(ultimateGames);
        classSelector = new ClassSelector(ultimateGames);

        ultimateGames.getGameItemManager()
                .registerGameItem(game, teamSelector)
                .registerGameItem(game, classSelector);

        setClassIcon(new ItemStack(Material.DIRT));
    }

    @SuppressWarnings("deprecation")
    @Override
    public void resetInventory(final Player player) {
        super.resetInventory(player);
        player.getInventory().addItem(classSelector.getItem());
        final String playerName = player.getName();
        Bukkit.getScheduler().scheduleSyncDelayedTask(ultimateGames.getPlugin(), new Runnable() {
            @Override
            public void run() {
                ArenaStatus status = ultimateGames.getPlayerManager().getPlayerArena(playerName).getStatus();
                if (status == ArenaStatus.OPEN || status == ArenaStatus.STARTING) {
                    player.getInventory().addItem(teamSelector.getItem());
                    player.updateInventory();
                }
            }
        }, 0);
        player.updateInventory();
    }
}
