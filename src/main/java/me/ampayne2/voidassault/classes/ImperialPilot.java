/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.classes;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.games.Game;
import me.ampayne2.ultimategames.api.games.blocks.GameBlock;
import me.ampayne2.ultimategames.api.games.items.GameItem;
import me.ampayne2.voidassault.VoidAssault;
import me.ampayne2.voidassault.items.placeables.Tb47TimeBomb;
import me.ampayne2.voidassault.items.tools.F187FusionCutter;
import me.ampayne2.voidassault.items.weapons.guns.Westar25CommandoPistol;
import me.ampayne2.voidassault.items.weapons.guns.bases.Gun;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Imperial pilot class.
 * http://battlefront.wikia.com/wiki/Imperial_Pilot
 * http://battlefront.wikia.com/wiki/Pilot_(Class)
 */
public class ImperialPilot extends VoidClass {
    private final Gun primary;
    private final GameItem fusionCutter;
    private final GameBlock timeBomb;
    private static final ItemStack ICON = new ItemStack(Material.FEATHER);

    public ImperialPilot(UltimateGames ultimateGames, Game game) {
        super(ultimateGames, game, "Imperial Pilot", Resistance.LIGHT, WalkSpeed.MEDIUM_FAST, 0, false);
        primary = new Westar25CommandoPistol(ultimateGames);
        fusionCutter = new F187FusionCutter(ultimateGames, ((VoidAssault) game.getGamePlugin()));
        timeBomb = new Tb47TimeBomb();

        ultimateGames.getGameItemManager()
                .registerGameItem(game, primary)
                .registerGameItem(game, fusionCutter);

        ultimateGames.getGameBlockManager()
                .registerGameBlock(game, timeBomb);

        setClassIcon(ICON);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void resetInventory(Player player) {
        super.resetInventory(player);
        ItemStack timeBombsItem = timeBomb.getItem();
        timeBombsItem.setAmount(3);
        player.getInventory().addItem(primary.getItem(), fusionCutter.getItem(), timeBombsItem);
        player.updateInventory();
    }
}
