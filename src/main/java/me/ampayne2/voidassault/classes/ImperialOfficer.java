/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.classes;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.games.Game;
import me.ampayne2.ultimategames.api.games.items.GameItem;
import me.ampayne2.ultimategames.api.utils.UGUtils;
import me.ampayne2.voidassault.items.consumables.Rage;
import me.ampayne2.voidassault.items.weapons.guns.SonicPistol;
import me.ampayne2.voidassault.items.weapons.guns.V6dMortarLauncher;
import me.ampayne2.voidassault.items.weapons.guns.bases.Gun;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Imperial officer class.
 * http://battlefront.wikia.com/wiki/Imperial_Officer
 * http://battlefront.wikia.com/wiki/Officer_(Class)
 */
public class ImperialOfficer extends VoidClass {
    private final Gun primary;
    private final Gun secondary;
    private final GameItem rage;
    private static final ItemStack ICON = new ItemStack(Material.NETHER_STAR);

    public ImperialOfficer(UltimateGames ultimateGames, Game game) {
        super(ultimateGames, game, "Imperial Officer", Resistance.LIGHT, WalkSpeed.MEDIUM, 0, false);
        primary = new SonicPistol(ultimateGames, 35);
        secondary = new V6dMortarLauncher(ultimateGames, 25);
        rage = new Rage();

        ultimateGames.getGameItemManager()
                .registerGameItem(game, primary)
                .registerGameItem(game, secondary)
                .registerGameItem(game, rage);

        setClassIcon(ICON);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void resetInventory(Player player) {
        super.resetInventory(player);
        player.getInventory().addItem(primary.getItem(), secondary.getItem(), rage.getItem(), primary.getAmmo(), secondary.getAmmo());
        player.updateInventory();
    }

    @Override
    public boolean hasAmmoSpace(Player player) {
        return UGUtils.getTotalItems(player.getInventory(), primary.getAmmoMaterial()) < 35;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void addAmmo(Player player) {
        ItemStack ammo = primary.getAmmo();
        ammo.setAmount(Math.min(35 - UGUtils.getTotalItems(player.getInventory(), primary.getAmmoMaterial()), 5));
        player.getInventory().addItem(ammo);
        player.updateInventory();
    }
}
