/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.classes;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.games.Game;
import me.ampayne2.ultimategames.api.games.items.GameItem;
import me.ampayne2.ultimategames.api.utils.UGUtils;
import me.ampayne2.voidassault.items.consumables.throwables.BcckThermalDetonator;
import me.ampayne2.voidassault.items.weapons.guns.E11BlasterRifle;
import me.ampayne2.voidassault.items.weapons.guns.Se14rBlasterPistol;
import me.ampayne2.voidassault.items.weapons.guns.bases.Gun;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Stormtrooper class.
 * http://battlefront.wikia.com/wiki/Stormtrooper
 * http://battlefront.wikia.com/wiki/Soldier_(Class)
 */
public class StormTrooper extends VoidClass {
    private final Gun primary;
    private final Gun secondary;
    private final GameItem grenade;
    private static final ItemStack ICON = new ItemStack(Material.BOW);

    public StormTrooper(UltimateGames ultimateGames, Game game) {
        super(ultimateGames, game, "Stormtrooper", Resistance.MEDIUM, WalkSpeed.MEDIUM, 0, false);
        primary = new E11BlasterRifle(ultimateGames, 250);
        secondary = new Se14rBlasterPistol(ultimateGames);
        grenade = new BcckThermalDetonator(ultimateGames);

        ultimateGames.getGameItemManager()
                .registerGameItem(game, primary)
                .registerGameItem(game, secondary);

        setClassIcon(ICON);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void resetInventory(Player player) {
        super.resetInventory(player);
        ItemStack grenadeItem = grenade.getItem();
        grenadeItem.setAmount(4);
        player.getInventory().addItem(primary.getItem(), secondary.getItem(), grenadeItem, primary.getAmmo());
        player.updateInventory();
    }

    @Override
    public boolean hasAmmoSpace(Player player) {
        return UGUtils.getTotalItems(player.getInventory(), primary.getAmmoMaterial()) < 250;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void addAmmo(Player player) {
        ItemStack ammo = primary.getAmmo();
        ammo.setAmount(Math.min(250 - UGUtils.getTotalItems(player.getInventory(), primary.getAmmoMaterial()), 50));
        player.getInventory().addItem(ammo);
        player.updateInventory();
    }
}
