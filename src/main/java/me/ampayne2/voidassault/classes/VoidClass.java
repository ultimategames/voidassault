/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.classes;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.arenas.ArenaStatus;
import me.ampayne2.ultimategames.api.games.Game;
import me.ampayne2.ultimategames.api.players.PlayerManager;
import me.ampayne2.ultimategames.api.players.classes.GameClass;
import me.ampayne2.ultimategames.api.utils.UGUtils;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

/**
 * Base class for void assault classes.
 * http://battlefront.wikia.com/wiki/Galactic_Empire
 * http://battlefront.wikia.com/wiki/Category:Classes
 */
public abstract class VoidClass extends GameClass {
    private final UltimateGames ultimateGames;
    private final Resistance resistance;
    private final WalkSpeed walkSpeed;
    private final int price;
    private final boolean isVip;

    public VoidClass(UltimateGames ultimateGames, Game game, String name, Resistance resistance, WalkSpeed walkSpeed, int price, boolean isVip) {
        super(ultimateGames, game, name, true);
        this.ultimateGames = ultimateGames;
        this.resistance = resistance;
        this.walkSpeed = walkSpeed;
        this.price = price;
        this.isVip = isVip;
    }

    public Resistance getResistance() {
        return resistance;
    }

    public WalkSpeed getWalkSpeed() {
        return walkSpeed;
    }

    public int getPrice() {
        return price;
    }

    public boolean isVip() {
        return isVip;
    }

    @Override
    public boolean addPlayer(Player player) {
        String playerName = player.getName();
        PlayerManager playerManager = ultimateGames.getPlayerManager();
        if (!hasPlayer(playerName) && playerManager.isPlayerInArena(playerName)) {
            Arena arena = playerManager.getPlayerArena(playerName);
            if (arena.getStatus() == ArenaStatus.RUNNING) {
                ultimateGames.getSpawnpointManager().getSpawnPoint(arena, ultimateGames.getTeamManager().getPlayerTeam(playerName).getName().equals("Empire") ? 0 : 1).teleportPlayer(player);
            }
        }
        return super.addPlayer(player);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void resetInventory(Player player) {
        player.setFlying(false);
        player.setAllowFlight(false);
        walkSpeed.setPlayerToSpeed(player);
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }
        player.setHealth(20.0);
        player.setFoodLevel(20);
        player.getInventory().clear();
        player.getInventory().addItem(UGUtils.createInstructionBook(getGame()));
        player.getInventory().setArmorContents(null);
        player.updateInventory();
    }

    public boolean hasAmmoSpace(Player player) {
        return false;
    }

    public void addAmmo(Player player) {
    }
}
