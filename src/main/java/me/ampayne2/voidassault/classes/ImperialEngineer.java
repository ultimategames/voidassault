/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.classes;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.games.Game;
import me.ampayne2.ultimategames.api.games.blocks.GameBlock;
import me.ampayne2.ultimategames.api.games.items.GameItem;
import me.ampayne2.ultimategames.api.utils.UGUtils;
import me.ampayne2.voidassault.VoidAssault;
import me.ampayne2.voidassault.items.consumables.throwables.AmmoDispenser;
import me.ampayne2.voidassault.items.consumables.throwables.HealthDispenser;
import me.ampayne2.voidassault.items.placeables.D5aDetpack;
import me.ampayne2.voidassault.items.tools.F187FusionCutter;
import me.ampayne2.voidassault.items.weapons.guns.E11eBlastCannon;
import me.ampayne2.voidassault.items.weapons.guns.bases.Gun;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Imperial engineer class.
 * http://battlefront.wikia.com/wiki/Imperial_Engineer
 * http://battlefront.wikia.com/wiki/Engineer_(Class)
 */
public class ImperialEngineer extends VoidClass {
    private final Gun primary;
    private final GameItem fusionCutter;
    private final GameBlock detpack;
    private final GameItem healthDispenser;
    private final GameItem ammoDispenser;
    private static final ItemStack ICON = new ItemStack(Material.BLAZE_ROD);

    public ImperialEngineer(UltimateGames ultimateGames, Game game) {
        super(ultimateGames, game, "Imperial Engineer", Resistance.MEDIUM, WalkSpeed.MEDIUM, 0, true);
        primary = new E11eBlastCannon(ultimateGames, 200);
        fusionCutter = new F187FusionCutter(ultimateGames, ((VoidAssault) game.getGamePlugin()));
        detpack = new D5aDetpack();
        healthDispenser = new HealthDispenser();
        ammoDispenser = new AmmoDispenser();

        ultimateGames.getGameItemManager()
                .registerGameItem(game, primary)
                .registerGameItem(game, fusionCutter)
                .registerGameItem(game, healthDispenser)
                .registerGameItem(game, ammoDispenser);

        ultimateGames.getGameBlockManager()
                .registerGameBlock(game, detpack);

        setClassIcon(ICON);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void resetInventory(Player player) {
        super.resetInventory(player);
        ItemStack healthDispenserItem = healthDispenser.getItem();
        healthDispenserItem.setAmount(5);
        ItemStack ammoDispenserItem = ammoDispenser.getItem();
        ammoDispenserItem.setAmount(5);
        ItemStack detpacksItem = detpack.getItem();
        detpacksItem.setAmount(3);
        player.getInventory().addItem(primary.getItem(), fusionCutter.getItem(), detpacksItem, healthDispenserItem, ammoDispenserItem, primary.getAmmo());
        player.updateInventory();
    }

    @Override
    public boolean hasAmmoSpace(Player player) {
        return UGUtils.getTotalItems(player.getInventory(), primary.getAmmoMaterial()) < 200;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void addAmmo(Player player) {
        ItemStack ammo = primary.getAmmo();
        ammo.setAmount(Math.min(200 - UGUtils.getTotalItems(player.getInventory(), primary.getAmmoMaterial()), 40));
        player.getInventory().addItem(ammo);
        player.updateInventory();
    }
}
