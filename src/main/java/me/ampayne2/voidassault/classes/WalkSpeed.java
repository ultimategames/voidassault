/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.classes;

import org.bukkit.entity.Player;

public enum WalkSpeed {
    SLOW(0.15F),
    MEDIUM_SLOW(0.175F),
    MEDIUM(0.2F),
    MEDIUM_FAST(0.225F),
    FAST(0.25F);

    /**
     * Movement speed of the WalkSpeed. Default movement is 0.2.
     */
    private float walkSpeed;

    private WalkSpeed(float walkSpeed) {
        this.walkSpeed = walkSpeed;
    }

    public void setPlayerToSpeed(Player player) {
        player.setWalkSpeed(walkSpeed);
    }

    public float getWalkSpeed() {
        return walkSpeed;
    }
}
