/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault;

import com.bergerkiller.bukkit.common.entity.CommonEntity;
import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.arenas.ArenaStatus;
import me.ampayne2.ultimategames.api.arenas.scoreboards.Scoreboard;
import me.ampayne2.ultimategames.api.arenas.spawnpoints.PlayerSpawnPoint;
import me.ampayne2.ultimategames.api.effects.GameSound;
import me.ampayne2.ultimategames.api.games.Game;
import me.ampayne2.ultimategames.api.games.GamePlugin;
import me.ampayne2.ultimategames.api.players.classes.GameClass;
import me.ampayne2.ultimategames.api.players.classes.GameClassManager;
import me.ampayne2.ultimategames.api.players.points.PointManager;
import me.ampayne2.ultimategames.api.players.teams.Team;
import me.ampayne2.ultimategames.api.players.teams.TeamManager;
import me.ampayne2.ultimategames.api.utils.UGUtils;
import me.ampayne2.voidassault.classes.*;
import me.ampayne2.voidassault.ships.ShipMinecart;
import me.ampayne2.voidassault.ships.TeamShip;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleBlockCollisionEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import java.util.*;

public class VoidAssault extends GamePlugin {
    private UltimateGames ultimateGames;
    private Game game;
    private Set<String> playersWithEmptyJumpPack = new HashSet<>();
    private Map<String, Integer> jumpPackTasks = new HashMap<>();
    private Map<Team, TeamShip> teamShips = new HashMap<>();
    private static final GameSound AMMO_DISPENSER_SOUND = new GameSound(Sound.ANVIL_BREAK, 1, 2);
    private static final GameSound HEALTH_DISPENSER_SOUND = new GameSound(Sound.BURP, 1, 1);
    private static final GameSound PLAYER_SHOT_SOUND = new GameSound(Sound.ARROW_HIT, 1, 2);
    private final static GameSound SHIP_GUN_SOUND = new GameSound(Sound.WOLF_WHINE, 1, 1.5F);

    @Override
    public boolean loadGame(UltimateGames ultimateGames, Game game) {
        this.ultimateGames = ultimateGames;
        this.game = game;

        ultimateGames.getGameClassManager()
                .registerGameClass(new DefaultClass(ultimateGames, game))
                .registerGameClass(new StormTrooper(ultimateGames, game))
                .registerGameClass(new ShockTrooper(ultimateGames, game))
                .registerGameClass(new ScoutTrooper(ultimateGames, game))
                .registerGameClass(new ImperialPilot(ultimateGames, game))
                .registerGameClass(new ImperialOfficer(ultimateGames, game))
                .registerGameClass(new ImperialMarine(ultimateGames, game))
                .registerGameClass(new ImperialEngineer(ultimateGames, game))
                .registerGameClass(new DarkTrooper(ultimateGames, game));

        return true;
    }

    @Override
    public void unloadGame() {
    }

    @Override
    public boolean reloadGame() {
        return true;
    }

    @Override
    public boolean stopGame() {
        return true;
    }

    @Override
    public boolean loadArena(Arena arena) {
        ultimateGames.addAPIHandler("/" + game.getName() + "/" + arena.getName(), new VoidAssaultWebHandler(ultimateGames, arena));
        TeamManager teamManager = ultimateGames.getTeamManager();
        teamManager.createTeam(ultimateGames, "Empire", arena, ChatColor.RED, false, true);
        teamManager.createTeam(ultimateGames, "Rebels", arena, ChatColor.BLUE, false, true);
        return true;
    }

    @Override
    public boolean unloadArena(Arena arena) {
        return true;
    }

    @Override
    public boolean isStartPossible(Arena arena) {
        return arena.getStatus() == ArenaStatus.OPEN;
    }

    @Override
    public boolean startArena(Arena arena) {
        return true;
    }

    @Override
    public boolean beginArena(Arena arena) {
        ultimateGames.getCountdownManager().createEndingCountdown(arena, ultimateGames.getConfigManager().getGameConfig(game).getInt("CustomValues.GameTime"), true);

        TeamManager teamManager = ultimateGames.getTeamManager();
        teamManager.sortPlayersIntoTeams(arena);
        Team empire = teamManager.getTeam(arena, "Empire");
        Team rebels = teamManager.getTeam(arena, "Rebels");
        Scoreboard scoreboard = ultimateGames.getScoreboardManager().createScoreboard(arena, "Void Assault");
        scoreboard.setScore(empire, 0);
        scoreboard.setScore(rebels, 0);
        scoreboard.setVisible(true);
        GameClassManager classManager = ultimateGames.getGameClassManager();
        PlayerSpawnPoint empireSpawn = ultimateGames.getSpawnpointManager().getSpawnPoint(arena, 0);
        empireSpawn.lock(false);
        for (String playerName : empire.getPlayers()) {
            Player player = Bukkit.getPlayerExact(playerName);
            GameClass playerClass = classManager.getPlayerClass(game, playerName);
            if (!playerClass.getName().equals("Default")) {
                empireSpawn.teleportPlayer(player);
            }
            scoreboard.addPlayer(player, empire);
            playerClass.resetInventory(player);
        }
        PlayerSpawnPoint rebelSpawn = ultimateGames.getSpawnpointManager().getSpawnPoint(arena, 1);
        rebelSpawn.lock(false);
        for (String playerName : rebels.getPlayers()) {
            Player player = Bukkit.getPlayerExact(playerName);
            GameClass playerClass = classManager.getPlayerClass(game, playerName);
            if (!playerClass.getName().equals("Default")) {
                rebelSpawn.teleportPlayer(player);
            }
            scoreboard.addPlayer(player, rebels);
            playerClass.resetInventory(player);
        }

        teamShips.put(empire, new TeamShip(ultimateGames, arena, empire));
        teamShips.put(rebels, new TeamShip(ultimateGames, arena, rebels));
        return true;
    }

    @Override
    public void endArena(Arena arena) {
        Scoreboard scoreboard = ultimateGames.getScoreboardManager().getScoreboard(arena);
        Team empire = ultimateGames.getTeamManager().getTeam(arena, "Empire");
        Team rebels = ultimateGames.getTeamManager().getTeam(arena, "Rebels");
        PointManager pointManager = ultimateGames.getPointManager();
        if (scoreboard.getScore(empire) == scoreboard.getScore(rebels)) {
            ultimateGames.getMessenger().sendGameMessage(Bukkit.getServer(), game, "Tie");
            for (String playerName : empire.getPlayers()) {
                pointManager.addPoint(game, playerName, "store", 25);
            }
            for (String playerName : rebels.getPlayers()) {
                pointManager.addPoint(game, playerName, "store", 25);
            }
        } else if (scoreboard.getScore(empire) > scoreboard.getScore(rebels)) {
            ultimateGames.getMessenger().sendGameMessage(Bukkit.getServer(), game, "Win", empire.getName());
            for (String playerName : empire.getPlayers()) {
                pointManager.addPoint(game, playerName, "store", 50);
            }
        } else {
            ultimateGames.getMessenger().sendGameMessage(Bukkit.getServer(), game, "Win", rebels.getName());
            for (String playerName : rebels.getPlayers()) {
                pointManager.addPoint(game, playerName, "store", 50);
            }
        }
        teamShips.get(empire).kill();
        teamShips.get(rebels).kill();
        teamShips.remove(empire);
        teamShips.remove(rebels);
    }

    @Override
    public boolean openArena(Arena arena) {
        return true;
    }

    @Override
    public boolean stopArena(Arena arena) {
        return true;
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean addPlayer(Player player, Arena arena) {
        if (arena.getStatus() == ArenaStatus.OPEN && arena.getPlayers().size() >= arena.getMinPlayers() && !ultimateGames.getCountdownManager().hasStartingCountdown(arena)) {
            ultimateGames.getCountdownManager().createStartingCountdown(arena, ultimateGames.getConfigManager().getGameConfig(game).getInt("CustomValues.StartWaitTime"));
        }
        player.teleport(ultimateGames.getLobbyManager().getLobby());
        ultimateGames.getGameClassManager().getGameClass(game, "Default").addPlayer(player, true, false);
        return true;
    }

    @Override
    public void removePlayer(Player player, Arena arena) {
        WalkSpeed.MEDIUM.setPlayerToSpeed(player);
        String playerName = player.getName();
        if (jumpPackTasks.containsKey(playerName)) {
            Bukkit.getServer().getScheduler().cancelTask(jumpPackTasks.get(playerName));
            jumpPackTasks.remove(playerName);
        }
        if (playersWithEmptyJumpPack.contains(playerName)) {
            playersWithEmptyJumpPack.remove(playerName);
        }
        player.setFlying(false);
        player.setAllowFlight(false);
        List<String> queuePlayer = ultimateGames.getQueueManager().getNextPlayers(1, arena);
        TeamManager teamManager = ultimateGames.getTeamManager();
        if (!queuePlayer.isEmpty()) {
            String newPlayerName = queuePlayer.get(0);
            Player newPlayer = Bukkit.getPlayerExact(newPlayerName);
            ultimateGames.getPlayerManager().addPlayerToArena(newPlayer, arena, true);
            Team team = teamManager.getPlayerTeam(playerName);
            if (team != null) {
                teamManager.setPlayerTeam(newPlayer, team);
            }
        }
        if (arena.getStatus() == ArenaStatus.RUNNING && (teamManager.getTeam(arena, "Empire").getPlayers().size() <= 0 || teamManager.getTeam(arena, "Rebels").getPlayers().size() <= 0)) {
            ultimateGames.getArenaManager().endArena(arena);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean addSpectator(Player player, Arena arena) {
        ultimateGames.getSpawnpointManager().getSpectatorSpawnPoint(arena).teleportPlayer(player);
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }
        player.setHealth(20.0);
        player.setFoodLevel(20);
        player.getInventory().clear();
        player.getInventory().addItem(UGUtils.createInstructionBook(game));
        player.getInventory().setArmorContents(null);
        player.updateInventory();
        return true;
    }

    @Override
    public void removeSpectator(Player player, Arena arena) {
    }

    @Override
    public void onPlayerDeath(Arena arena, PlayerDeathEvent event) {
        Player player = event.getEntity();
        String playerName = player.getName();
        if (jumpPackTasks.containsKey(playerName)) {
            Bukkit.getServer().getScheduler().cancelTask(jumpPackTasks.get(playerName));
            jumpPackTasks.remove(playerName);
            player.setFlying(false);
        }
        if (playersWithEmptyJumpPack.contains(playerName)) {
            playersWithEmptyJumpPack.remove(playerName);
        }
        Player killer = player.getKiller();
        Scoreboard scoreboard = ultimateGames.getScoreboardManager().getScoreboard(arena);
        if (killer != null) {
            String killerName = killer.getName();
            ultimateGames.getMessenger().sendGameMessage(arena, game, "Kill", playerName, killerName);
            Team killerTeam = ultimateGames.getTeamManager().getPlayerTeam(killerName);
            scoreboard.setScore(killerTeam, scoreboard.getScore(killerTeam) + 1);
            checkForWin(arena);
        } else {
            ultimateGames.getMessenger().sendGameMessage(arena, game, "Death", playerName);
            Team playerTeam = ultimateGames.getTeamManager().getPlayerTeam(playerName);
            scoreboard.setScore(playerTeam, scoreboard.getScore(playerTeam) + 1);
        }
        event.getDrops().clear();
        UGUtils.autoRespawn(ultimateGames.getPlugin(), event.getEntity());
    }

    @Override
    public void onPlayerRespawn(Arena arena, PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        String playerName = player.getName();
        event.setRespawnLocation(ultimateGames.getLobbyManager().getLobby());
        GameClass defaultClass = ultimateGames.getGameClassManager().getGameClass(game, "Default");
        if (defaultClass.hasPlayer(playerName)) {
            defaultClass.resetInventory(player);
        } else {
            defaultClass.addPlayer(player, true, false);
        }
    }

    @Override
    public void onEntityDamage(Arena arena, EntityDamageEvent event) {
        if (arena.getStatus() == ArenaStatus.RUNNING && event.getEntity() instanceof Player && isDamageCauseAllowed(event.getCause())) {
            Player player = (Player) event.getEntity();
            VoidClass playerClass = (VoidClass) ultimateGames.getGameClassManager().getPlayerClass(game, player.getName());
            if (!playerClass.getName().equals("Default")) {
                player.damage(event.getDamage() * playerClass.getResistance().getResistanceModifier());
            }
        }
        event.setCancelled(true);
    }

    @Override
    public void onEntityDamageByEntity(Arena arena, EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Projectile && event.getEntity() instanceof Player) {
            Projectile projectile = (Projectile) event.getDamager();
            Player player = (Player) event.getEntity();
            String playerName = player.getName();
            if (ultimateGames.getPlayerManager().isPlayerInArena(playerName) && ultimateGames.getPlayerManager().getPlayerArena(playerName).getGame().equals(game)) {
                // TODO: Check if the projectile is a gun projectile, if so cancel event and modify damage
                // TODO: Play player shot GameSound
                player.damage(1);
                PLAYER_SHOT_SOUND.play(player.getLocation());
            }
        }
        event.setCancelled(true);
    }

    @Override
    public void onPlayerMove(Arena arena, PlayerMoveEvent event) {
        Player player = event.getPlayer();
        String playerName = player.getName();
        if (arena.getStatus() == ArenaStatus.RUNNING && ((Entity) player).isOnGround()) {
            if (player.getLocation().getBlock().getType().equals(Material.IRON_PLATE)) {
                Location location = player.getLocation();
                location.getBlock().setType(Material.AIR);
                location.getWorld().createExplosion(location.getX(), location.getY(), location.getZ(), 4F, false, false);
            } else if (ultimateGames.getGameClassManager().getPlayerClass(game, playerName).getName().equals("Dark Trooper")) {
                if (playersWithEmptyJumpPack.contains(playerName)) {
                    playersWithEmptyJumpPack.remove(playerName);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onVehicleEnter(VehicleEnterEvent event) {
        if (event.getEntered() instanceof Player && event.getVehicle() instanceof Minecart) {
            String playerName = ((Player) event.getEntered()).getName();
            if (ultimateGames.getPlayerManager().isPlayerInArena(playerName)) {
                Arena arena = ultimateGames.getPlayerManager().getPlayerArena(playerName);
                if (arena.getGame().equals(game)) {
                    if (arena.getStatus() == ArenaStatus.RUNNING) {
                        final Vehicle vehicle = event.getVehicle();
                        Bukkit.getScheduler().scheduleSyncDelayedTask(ultimateGames.getPlugin(), new Runnable() {
                            @Override
                            public void run() {
                                CommonEntity<?> entity = CommonEntity.get(vehicle);
                                if (!(entity.getController().getClass() == ShipMinecart.class)) {
                                    entity.setController(new ShipMinecart(ultimateGames, game));
                                }
                            }
                        }, 0);
                    } else {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onVehicleCollide(VehicleBlockCollisionEvent event) {
        Vehicle vehicle = event.getVehicle();
        Entity passenger = vehicle.getPassenger();
        if (vehicle instanceof Minecart && passenger != null && vehicle.getPassenger() instanceof Player) {
            Player player = (Player) passenger;
            String playerName = player.getName();
            if (ultimateGames.getPlayerManager().isPlayerInArena(playerName)) {
                Arena arena = ultimateGames.getPlayerManager().getPlayerArena(playerName);
                if (arena.getGame().equals(game) && arena.getStatus() == ArenaStatus.RUNNING) {
                    Minecart minecart = (Minecart) event.getVehicle();
                    double newDamage = minecart.getDamage() + 3;
                    if (newDamage >= 40) {
                        minecart.setPassenger(null);
                        minecart.remove();
                        player.damage(player.getHealth());
                        Location location = player.getLocation();
                        location.getWorld().createExplosion(location.getX(), location.getY(), location.getZ(), 4F, false, false);
                    } else {
                        minecart.setDamage(newDamage);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onFlightToggle(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();
        final String playerName = player.getName();
        if (player.getGameMode() != GameMode.CREATIVE && ultimateGames.getPlayerManager().isPlayerInArena(playerName)) {
            Arena arena = ultimateGames.getPlayerManager().getPlayerArena(playerName);
            if (arena.getGame().equals(game)) {
                event.setCancelled(true);
                if (arena.getStatus() == ArenaStatus.RUNNING && ultimateGames.getGameClassManager().getPlayerClass(game, playerName).getName().equals("Dark Trooper") && !player.isFlying() && !playersWithEmptyJumpPack.contains(playerName) && !jumpPackTasks.containsKey(playerName)) {
                    player.setFlying(true);
                    jumpPackTasks.put(playerName, Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(ultimateGames.getPlugin(), new Runnable() {
                        @Override
                        public void run() {
                            if (ultimateGames.getPlayerManager().isPlayerInArena(playerName) && ultimateGames.getPlayerManager().getPlayerArena(playerName).getGame().equals(game) && ultimateGames.getGameClassManager().getPlayerClass(game, playerName).getName().equals("Dark Trooper")) {
                                Bukkit.getPlayerExact(playerName).setFlying(false);
                                playersWithEmptyJumpPack.add(playerName);
                                jumpPackTasks.remove(playerName);
                            }
                        }
                    }, 40));
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockIgnite(BlockIgniteEvent event) {
        if (event.getIgnitingBlock() != null) {
            Arena arena = ultimateGames.getArenaManager().getLocationArena(event.getIgnitingBlock().getLocation());
            if (arena != null && arena.getGame().equals(game)) {
                event.setCancelled(true);
            }
        }
    }

    @Override
    public void onItemPickup(Arena arena, PlayerPickupItemEvent event) {
        if (arena.getStatus() == ArenaStatus.RUNNING && !event.getItem().hasMetadata("no_pickup")) {
            Item item = event.getItem();
            ItemStack itemStack = event.getItem().getItemStack();
            Player player = event.getPlayer();
            VoidClass voidClass = (VoidClass) ultimateGames.getGameClassManager().getPlayerClass(game, player.getName());
            if (itemStack.getType() == Material.ARROW && voidClass.hasAmmoSpace(player)) {
                voidClass.addAmmo(player);
                AMMO_DISPENSER_SOUND.play(player, player.getLocation());
                if (itemStack.getAmount() > 1) {
                    itemStack.setAmount(itemStack.getAmount() - 1);
                    item.setItemStack(itemStack);
                } else {
                    event.getItem().remove();
                }
            } else if (itemStack.getType() == Material.GOLDEN_APPLE) {
                if (player.getHealth() < 20) {
                    player.setHealth(Math.min(player.getHealth() + 5, 20));
                    HEALTH_DISPENSER_SOUND.play(player, player.getLocation());
                    if (itemStack.getAmount() > 1) {
                        itemStack.setAmount(itemStack.getAmount() - 1);
                        item.setItemStack(itemStack);
                    } else {
                        event.getItem().remove();
                    }
                }
            }
        }
        event.setCancelled(true);
    }

    @Override
    public void onItemDrop(Arena arena, PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @Override
    public void onPlayerFoodLevelChange(Arena arena, FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @Override
    public void onPlayerInteract(Arena arena, PlayerInteractEvent event) {
        ItemStack item = event.getItem();
        if (item != null) {
            Material type = item.getType();
            if (type == Material.SNOW_BALL || type == Material.EGG) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.getVehicle() != null) {
            Entity vehicle = player.getVehicle();
            Location vehicleLocation = vehicle.getLocation();
            Vector vehicleDirection = vehicleLocation.getDirection();
            double x = vehicleDirection.getZ();
            double z = -vehicleDirection.getX();
            Vector arrowDirection = new Vector(x, vehicleDirection.getY(), z).multiply(5);
            Arrow arrow = vehicleLocation.getWorld().spawnArrow(vehicleLocation.clone().add(arrowDirection.normalize()), arrowDirection, (float) arrowDirection.length(), 0);
            arrow.setMetadata("no_pickup", new FixedMetadataValue(ultimateGames.getPlugin(), true));
            event.setCancelled(true);
            SHIP_GUN_SOUND.play(vehicleLocation);
        }
    }

    public boolean isDamageCauseAllowed(EntityDamageEvent.DamageCause cause) {
        switch (cause) {
            case ENTITY_EXPLOSION:
            case BLOCK_EXPLOSION:
            case CUSTOM:
            case DROWNING:
            case FIRE:
            case FIRE_TICK:
            case LAVA:
            case LIGHTNING:
            case POISON:
            case SUFFOCATION:
            case SUICIDE:
            case VOID:
                return true;
            default:
                return false;
        }
    }

    public void checkForWin(Arena arena) {
        Scoreboard scoreboard = ultimateGames.getScoreboardManager().getScoreboard(arena);
        if (scoreboard != null) {
            Team empire = ultimateGames.getTeamManager().getTeam(arena, "Empire");
            Team rebels = ultimateGames.getTeamManager().getTeam(arena, "Rebels");
            if (scoreboard.getScore(empire) >= 250 || scoreboard.getScore(rebels) >= 250) {
                ultimateGames.getArenaManager().endArena(arena);
            }
        }
    }

    public TeamShip getTeamShip(Team team) {
        return teamShips.get(team);
    }
}
