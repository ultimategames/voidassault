/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.placeables;

import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.games.blocks.GameBlock;
import org.bukkit.Material;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * TB-47 Time Bomb. http://battlefront.wikia.com/wiki/Time_Bomb
 */
public class Tb47TimeBomb extends GameBlock {
    private static final ItemStack ITEM;

    public Tb47TimeBomb() {
        super(ITEM, false);
    }

    @Override
    public boolean place(Arena arena, BlockPlaceEvent event) {
        // TODO: Schedule a timer for the time bomb to explode.
        return true;
    }

    static {
        ITEM = new ItemStack(Material.NOTE_BLOCK);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName("TB-47 Time Bomb");
        ITEM.setItemMeta(meta);
    }
}
