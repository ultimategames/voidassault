/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.weapons.guns.bases;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.effects.GameSound;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class SonicGun extends Gun {
    private final static GameSound SOUND = new GameSound(Sound.ZOMBIE_REMEDY, 1, 2);

    public SonicGun(UltimateGames ultimateGames, ItemStack item, ItemStack ammo, long fireSpeed) {
        super(ultimateGames, item, ammo, fireSpeed);
    }

    @Override
    public boolean click(Arena arena, PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (canShoot(arena, player)) {
            // TODO: Shoot a wave of sound
            SOUND.play(player.getLocation());
            super.click(arena, event);
        }
        return true;
    }
}
