/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.weapons.guns;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.voidassault.items.weapons.guns.bases.BasicGun;
import org.bukkit.Material;
import org.bukkit.entity.Fireball;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * MiniMag PTL Rocket Launcher. http://battlefront.wikia.com/wiki/MiniMag_PTL_Rocket_Launcher
 */
public class MiniMagPtlRocketLauncher extends BasicGun {
    private final static ItemStack ITEM;

    public MiniMagPtlRocketLauncher(UltimateGames ultimateGames, int initialAmmo) {
        super(ultimateGames, ITEM, new ItemStack(Material.FIREBALL, initialAmmo), Fireball.class, 2, 10);
    }

    static {
        // TODO: Choose item for gun
        ITEM = new ItemStack(Material.IRON_SWORD);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName("MiniMag PTL Rocket Launcher");
        ITEM.setItemMeta(meta);
    }
}
