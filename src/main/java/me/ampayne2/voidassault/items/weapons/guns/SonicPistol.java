/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.weapons.guns;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.voidassault.items.weapons.guns.bases.SonicGun;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Sonic Pistol. http://battlefront.wikia.com/wiki/Sonic_Pistol
 */
public class SonicPistol extends SonicGun {
    private final static ItemStack ITEM;

    public SonicPistol(UltimateGames ultimateGames, int initialAmmo) {
        super(ultimateGames, ITEM, new ItemStack(Material.SNOW_BALL, initialAmmo), 4);
    }

    static {
        ITEM = new ItemStack(Material.FEATHER);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName("Sonic Pistol");
        ITEM.setItemMeta(meta);
    }
}
