/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.weapons.guns.bases;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.effects.GameSound;
import me.ampayne2.ultimategames.api.games.items.RepeatingGameItem;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

// TODO: Add gun knockback
public class Gun extends RepeatingGameItem {
    private final ItemStack ammo;
    private final Material ammoMaterial;
    private static final GameSound NO_AMMO_SOUND = new GameSound(Sound.CLICK, 1, 2);

    public Gun(UltimateGames ultimateGames, ItemStack item, ItemStack ammo, long fireSpeed) {
        super(ultimateGames, item, fireSpeed);
        this.ammo = ammo;
        this.ammoMaterial = ammo == null ? null : ammo.getType();
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean click(Arena arena, PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if (ammoMaterial != null) {
            player.getInventory().removeItem(new ItemStack(ammoMaterial, 1));
            player.updateInventory();
        }

        return super.click(arena, event);
    }

    public boolean canShoot(Arena arena, Player player) {
        String playerName = player.getName();
        if (canClick(arena, playerName)) {
            if (!(ammoMaterial == null || player.getInventory().contains(ammoMaterial))) {
                NO_AMMO_SOUND.play(player, player.getLocation());
                return false;
            }
            return true;
        }
        return false;
    }

    public ItemStack getAmmo() {
        return ammo == null ? null : ammo.clone();
    }

    public Material getAmmoMaterial() {
        return ammoMaterial;
    }
}
