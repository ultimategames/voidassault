/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.weapons.guns.bases;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.effects.GameSound;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

public class GrenadeGun extends Gun {
    private final UltimateGames ultimateGames;
    private final static GameSound SOUND = new GameSound(Sound.CHEST_OPEN, 1, 1.7F);

    public GrenadeGun(UltimateGames ultimateGames, ItemStack item, ItemStack ammo, long fireSpeed) {
        super(ultimateGames, item, ammo, fireSpeed);
        this.ultimateGames = ultimateGames;
    }

    @Override
    public boolean click(Arena arena, PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (canShoot(arena, player)) {
            final Entity grenade = event.getPlayer().getWorld().dropItem(event.getPlayer().getEyeLocation(), new ItemStack(getAmmoMaterial()));
            grenade.setVelocity(player.getEyeLocation().getDirection());
            grenade.setMetadata("no_pickup", new FixedMetadataValue(ultimateGames.getPlugin(), true));
            SOUND.play(player.getLocation());
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(ultimateGames.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    if (!grenade.isDead()) {
                        Location location = grenade.getLocation();
                        grenade.remove();
                        location.getWorld().createExplosion(location.getX(), location.getY(), location.getZ(), 4F, false, false);
                    }
                }
            }, 60);
            super.click(arena, event);
        }
        return true;
    }
}
