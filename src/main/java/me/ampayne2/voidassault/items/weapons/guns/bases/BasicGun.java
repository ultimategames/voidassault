/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.weapons.guns.bases;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.effects.GameSound;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

public class BasicGun extends Gun {
    private final UltimateGames ultimateGames;
    private final Class<? extends Projectile> projectile;
    private final int velocity;
    private final static GameSound SOUND = new GameSound(Sound.WOLF_WHINE, 1, 1.8F);

    public BasicGun(UltimateGames ultimateGames, ItemStack item, ItemStack ammo, Class<? extends Projectile> projectile, int velocity, long fireSpeed) {
        super(ultimateGames, item, ammo, fireSpeed);
        this.ultimateGames = ultimateGames;
        this.projectile = projectile;
        this.velocity = velocity;
    }

    @Override
    public boolean click(Arena arena, PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (canShoot(arena, player)) {
            Projectile launchedProjectile = player.launchProjectile(projectile);
            launchedProjectile.getVelocity().multiply(velocity);
            launchedProjectile.setMetadata("no_pickup", new FixedMetadataValue(ultimateGames.getPlugin(), true));
            SOUND.play(player.getLocation());
            super.click(arena, event);
        }
        return true;
    }
}
