/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.weapons.guns;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.voidassault.items.weapons.guns.bases.GrenadeGun;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * V-6D Mortar Launcher. http://battlefront.wikia.com/wiki/Mortar_Launcher
 */
public class V6dMortarLauncher extends GrenadeGun {
    private final static ItemStack ITEM;

    public V6dMortarLauncher(UltimateGames ultimateGames, int initialAmmo) {
        super(ultimateGames, ITEM, new ItemStack(Material.EGG, initialAmmo), 100);
    }

    static {
        ITEM = new ItemStack(Material.FLINT_AND_STEEL);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName("V-6D Mortar Launcher");
        ITEM.setItemMeta(meta);
    }
}
