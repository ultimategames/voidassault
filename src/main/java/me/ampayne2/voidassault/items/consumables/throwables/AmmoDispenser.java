/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.consumables.throwables;

import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.effects.GameSound;
import me.ampayne2.ultimategames.api.games.items.ThrowableGameItem;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Ammo dispenser. http://battlefront.wikia.com/wiki/Health_and_Ammo_Dispenser
 */
public class AmmoDispenser extends ThrowableGameItem {
    private static final ItemStack ITEM;
    private final static GameSound THROW_SOUND = new GameSound(Sound.IRONGOLEM_THROW, 1, 1);

    public AmmoDispenser() {
        super(ITEM);
    }

    @Override
    public void onItemThrow(Arena arena, Item item) {
        THROW_SOUND.play(item.getLocation());
    }

    static {
        ITEM = new ItemStack(Material.ARROW);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName("Ammo Dispenser");
        ITEM.setItemMeta(meta);
    }
}
