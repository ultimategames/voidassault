/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.consumables;

import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.arenas.ArenaStatus;
import me.ampayne2.ultimategames.api.effects.GameSound;
import me.ampayne2.ultimategames.api.games.items.GameItem;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Rage. http://battlefront.wikia.com/wiki/Rage
 */
public class Rage extends GameItem {
    private static final ItemStack ITEM;
    private static final GameSound SOUND = new GameSound(Sound.WITHER_SPAWN, 1, 1);

    public Rage() {
        super(ITEM, true);
    }

    @Override
    public boolean click(Arena arena, PlayerInteractEvent event) {
        if (arena.getStatus() == ArenaStatus.RUNNING) {
            // TODO: Perform rage action
            SOUND.play(event.getPlayer().getLocation());
            return true;
        } else {
            return false;
        }
    }

    static {
        ITEM = new ItemStack(Material.REDSTONE);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName("Rage");
        ITEM.setItemMeta(meta);
    }
}
