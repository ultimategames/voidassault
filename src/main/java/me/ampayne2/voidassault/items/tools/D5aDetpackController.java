/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.tools;

import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.games.items.GameItem;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class D5aDetpackController extends GameItem {
    private static final ItemStack ITEM;

    public D5aDetpackController() {
        super(ITEM, false);
    }

    @Override
    public boolean click(Arena arena, PlayerInteractEvent event) {
        // TODO: Explode the player's placed detpacks
        return true;
    }

    static {
        ITEM = new ItemStack(Material.REDSTONE_TORCH_ON);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName("D5a Detpack Controller");
        ITEM.setItemMeta(meta);
    }
}
