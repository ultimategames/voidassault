/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.items.tools;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.arenas.ArenaStatus;
import me.ampayne2.ultimategames.api.effects.GameSound;
import me.ampayne2.ultimategames.api.games.items.RepeatingGameItem;
import me.ampayne2.voidassault.VoidAssault;
import me.ampayne2.voidassault.ships.ShipZone;
import me.ampayne2.voidassault.ships.TeamShip;
import me.ampayne2.voidassault.ships.ZoneType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * F-187 Fusion Cutter. http://battlefront.wikia.com/wiki/Fusion_Cutter
 */
public class F187FusionCutter extends RepeatingGameItem {
    private final UltimateGames ultimateGames;
    private final VoidAssault voidAssault;
    private static final ItemStack ITEM;
    private static final GameSound HEAL_SOUND = new GameSound(Sound.FIZZ, 1, 2);

    public F187FusionCutter(UltimateGames ultimateGames, VoidAssault voidAssault) {
        super(ultimateGames, ITEM, 5);
        this.ultimateGames = ultimateGames;
        this.voidAssault = voidAssault;
    }

    @Override
    public boolean click(Arena arena, PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String playerName = player.getName();
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && arena.getStatus() == ArenaStatus.RUNNING && canClick(arena, playerName)) {
            TeamShip playerShip = voidAssault.getTeamShip(ultimateGames.getTeamManager().getPlayerTeam(playerName));
            Location location = event.getClickedBlock().getLocation();
            for (ZoneType zoneType : ZoneType.class.getEnumConstants()) {
                ShipZone zone = playerShip.getZone(zoneType);
                if (zone.getZone().isLocationInZone(location)) {
                    zone.heal();
                    HEAL_SOUND.play(location);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    static {
        ITEM = new ItemStack(Material.BLAZE_ROD);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName("F-187 Fusion Cutter");
        ITEM.setItemMeta(meta);
    }
}
