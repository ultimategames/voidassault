/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.ships;

public enum ZoneType {
    SHIELDS("shields", "Shield Generator"),
    LIFE_SUPPORT("lifesupport", "Life Support Room"),
    ENGINES("engines", "Engine Room"),
    BRIDGE("bridge", "Bridge"),
    COMMS("comms", "Comm Array"),
    SENSORS("sensors", "Sensor System"),
    TURRETS("turrets", "Auto-Turret CPU");

    private final String zoneName;
    private final String displayName;

    private ZoneType(String zoneName, String displayName) {
        this.zoneName = zoneName;
        this.displayName = displayName;
    }

    public String getZoneName() {
        return zoneName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
