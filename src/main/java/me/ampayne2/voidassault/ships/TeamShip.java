/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.ships;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.arenas.zones.ZoneManager;
import me.ampayne2.ultimategames.api.players.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.Map;

public class TeamShip {
    private final Arena arena;
    private final Team team;
    private final Map<ZoneType, ShipZone> zones = new HashMap<>();
    private final int taskId;

    public TeamShip(final UltimateGames ultimateGames, final Arena arena, final Team team) {
        this.arena = arena;
        this.team = team;

        ZoneManager zoneManager = ultimateGames.getZoneManager();
        String teamName = team.getName().toLowerCase();
        for (ZoneType zoneType : ZoneType.class.getEnumConstants()) {
            zones.put(zoneType, new ShipZone(ultimateGames, this, zoneManager.getZone(arena, teamName + zoneType.getZoneName()), zoneType));
        }

        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(ultimateGames.getPlugin(), new Runnable() {
            @Override
            public void run() {
                for (Team arenaTeam : ultimateGames.getTeamManager().getTeamsOfArena(arena)) {
                    if (!arenaTeam.equals(team)) {
                        for (String playerName : arenaTeam.getPlayers()) {
                            Location location = Bukkit.getPlayerExact(playerName).getLocation();
                            for (ShipZone shipZone : zones.values()) {
                                if (shipZone.getZone().isLocationInZone(location)) {
                                    shipZone.damage(arenaTeam);
                                }
                            }
                        }
                    }
                }
            }
        }, 20, 20);
    }

    public Arena getArena() {
        return arena;
    }

    public Team getTeam() {
        return team;
    }

    public ShipZone getZone(ZoneType zoneType) {
        return zones.get(zoneType);
    }

    public void kill() {
        Bukkit.getScheduler().cancelTask(taskId);
    }
}
