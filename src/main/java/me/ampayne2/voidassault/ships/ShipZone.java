/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.ships;

import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.arenas.Arena;
import me.ampayne2.ultimategames.api.arenas.scoreboards.Scoreboard;
import me.ampayne2.ultimategames.api.arenas.zones.Zone;
import me.ampayne2.ultimategames.api.games.Game;
import me.ampayne2.ultimategames.api.players.points.PointManager;
import me.ampayne2.ultimategames.api.players.teams.Team;
import me.ampayne2.voidassault.VoidAssault;

public class ShipZone {
    private final UltimateGames ultimateGames;
    private final Game game;
    private final Arena arena;
    private final TeamShip ship;
    private final Zone zone;
    private final ZoneType zoneType;
    private int health = 100;

    public ShipZone(UltimateGames ultimateGames, TeamShip ship, Zone zone, ZoneType zoneType) {
        this.ultimateGames = ultimateGames;
        this.game = ship.getArena().getGame();
        this.arena = ship.getArena();
        this.ship = ship;
        this.zone = zone;
        this.zoneType = zoneType;
    }

    public TeamShip getShip() {
        return ship;
    }

    public Zone getZone() {
        return zone;
    }

    public ZoneType getZoneType() {
        return zoneType;
    }

    public boolean isAlive() {
        return health > 0;
    }

    public int getHealth() {
        return health;
    }

    public void heal() {
        if (!isAlive()) {
            ultimateGames.getMessenger().sendGameMessage(ship.getArena(), ship.getArena().getGame(), "Repaired", ship.getTeam().getName(), zoneType.getDisplayName());
        }
        health = Math.min(health + 1, 100);
    }

    public void damage(Team damager) {
        if (isAlive()) {
            if (!zoneType.equals(ZoneType.SHIELDS) && ship.getZone(ZoneType.SHIELDS).isAlive()) {
                ship.getZone(ZoneType.SHIELDS).damage(damager);
            } else {
                health--;
            }
            if (health == 25) {
                ultimateGames.getMessenger().sendGameMessage(arena, game, "Critical", ship.getTeam().getName(), zoneType.getDisplayName());
            } else if (health == 0) {
                ultimateGames.getMessenger().sendGameMessage(arena, game, "Destroyed", ship.getTeam().getName(), zoneType.getDisplayName());
                Scoreboard scoreboard = ultimateGames.getScoreboardManager().getScoreboard(arena);
                scoreboard.setScore(damager, scoreboard.getScore(damager) + 50);
                PointManager pointManager = ultimateGames.getPointManager();
                for (String playerName : damager.getPlayers()) {
                    pointManager.addPoint(game, playerName, "store", 25);
                }
                ((VoidAssault) game.getGamePlugin()).checkForWin(arena);
            }
        }
    }
}
