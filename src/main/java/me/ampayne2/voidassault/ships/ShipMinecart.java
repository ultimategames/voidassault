/*
 * This file is part of VoidAssault.
 *
 * Copyright (c) 2013-2013, UltimateGames <http://bitbucket.org/ultimategames/>
 *
 * VoidAssault is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoidAssault is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with VoidAssault.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.ampayne2.voidassault.ships;

import com.bergerkiller.bukkit.common.controller.EntityController;
import com.bergerkiller.bukkit.common.entity.type.CommonMinecartRideable;
import me.ampayne2.ultimategames.api.UltimateGames;
import me.ampayne2.ultimategames.api.effects.ParticleEffect;
import me.ampayne2.ultimategames.api.games.Game;
import me.ampayne2.ultimategames.api.players.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class ShipMinecart extends EntityController<CommonMinecartRideable> {
    private final UltimateGames ultimateGames;
    private final Game game;
    private boolean regenerating;
    private long ticksHadPlayer = 0;
    private Vector momentum;
    private static final double MAX_SPEED = 1.0;

    public ShipMinecart(UltimateGames ultimateGames, Game game) {
        this.ultimateGames = ultimateGames;
        this.game = game;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onTick() {
        if (entity.hasPlayerPassenger()) {
            final Player player = entity.getPlayerPassenger();
            final Location eyeLocation = player.getEyeLocation();
            final Vector eyeDirection = eyeLocation.getDirection();

            // Setup minecart when player enters
            if (ticksHadPlayer == 0) {
                momentum = entity.vel.vector().clone();
                regenerating = ultimateGames.getGameClassManager().getPlayerClass(game, player.getName()).getName().equals("Imperial Pilot");
                Team team = ultimateGames.getTeamManager().getPlayerTeam(player.getName());
                if (team != null) {
                    entity.setBlock(Material.WOOL, team.getName().equals("Empire") ? DyeColor.RED.getData() : DyeColor.BLUE.getData());
                } else {
                    entity.setBlock(Material.WOOL);
                }
                entity.setBlockOffset(32);
            }

            if (ticksHadPlayer < 50) {
                this.onMove(0.1, 0.1, 0.1);
                entity.setRotation(eyeLocation.getYaw() - 90, eyeLocation.getPitch());
            } else {
                // Push minecart in player's eye direction
                momentum.add(eyeDirection.clone().multiply(0.1));

                // Limit the minecart's speed
                if (momentum.length() > MAX_SPEED) {
                    momentum.normalize();
                    momentum.multiply(MAX_SPEED);
                }

                // Move the minecart and set rotation to same pitch/yaw as eye location
                this.onMove(momentum.getX(), momentum.getY(), momentum.getZ());
                entity.setRotation(eyeLocation.getYaw() - 90, eyeLocation.getPitch());

                // Play a firework spark particle every other tick
                if (ticksHadPlayer % 2 == 0) {
                    final Location location = entity.getLocation();
                    Bukkit.getScheduler().scheduleSyncDelayedTask(ultimateGames.getPlugin(), new Runnable() {
                        @Override
                        public void run() {
                            ParticleEffect.FIREWORKS_SPARK.display(location, 0, 0, 0, 0, 1);
                        }
                    }, 8);
                }

                // Heal the minecart
                if (regenerating) {
                    double newDamage = entity.getDamage() - 1;
                    entity.setDamage(newDamage < 0 ? 0 : newDamage);
                }
            }

            ticksHadPlayer++;
        } else {
            if (ticksHadPlayer != 0) {
                entity.vel.set(momentum);
                ticksHadPlayer = 0;
                regenerating = false;
            }
            super.onTick();
        }
    }

    @Override
    public boolean onBlockCollision(Block block, BlockFace hitFace) {
        if (entity.hasPlayerPassenger()) {
            double newDamage = entity.getDamage() + 1;
            if (newDamage >= 40) {
                entity.eject();
                Location location = entity.getLocation();
                entity.remove();
                location.getWorld().createExplosion(location.getX(), location.getY(), location.getZ(), 4F, false, false);
            } else {
                entity.setDamage(newDamage);
            }
        }
        return true;
    }

    @Override
    public void onBurnDamage(double damage) {
    }
}
